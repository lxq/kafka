var kafka = require('kafka-node');
var Producer = kafka.Producer;
var Client = kafka.Client;
var client = new Client('localhost:2181');

// For this demo we just log client errors to the console.
client.on('error', function(error) {
  console.error(error);
});
var producer = new Producer(client);

producer.on('ready', function() {
  // Create a new payload
  var payload = [{                     //该数组中可以写入多个json对象，即多个topic和message
    topic: 'node-test',
    messages: JSON.stringify({applianceId: "12345", deviceType: "DB", stamp: "1234567"})   //message的值为字符串或者字符串数组，所以此处必须转
  }];

  //Send payload to Kafka and log result/error
  producer.send(payload, function(error, result) {
    console.info('Sent payload to Kafka: ', payload);
    if (error) {
      console.error(error);
    } else {
      console.log('result: ', result);
    }
  });
});

// For this demo we just log producer errors to the console.
producer.on('error', function(error) {
  console.error(error);
});
