'use strict';

var kafka = require('kafka-node');
var Consumer = kafka.Consumer;
var Client = kafka.Client;

var client = new Client('localhost:2181');
var topics = [
  {topic: 'node-test', offset: 2}    //producer每发布一次，offset会自增1，这里可以指定接受消息的位置
];
var options = { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024, fromOffset: true };    //fromOffset置位true

var consumer = new Consumer(client, topics, options);

consumer.on('message', function (message) {
  console.log('message=', JSON.parse(message.value));     

});

consumer.on('error', function (err) {
  console.log('error', err);
});


